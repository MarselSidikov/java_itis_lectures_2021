package ru.itis.di.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.di.services.RandomNumbersService;
import ru.itis.di.services.RandomNumbersServiceImpl;

/**
 * 13.03.2021
 * 02. di-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class RandomNumbersController {

    @Autowired
    @Qualifier(value = "randomNumbersWithRangeService")
    private RandomNumbersService service;

    @GetMapping("/numbers")
    public String getNumbersPage(Model model) {
        model.addAttribute("randomNumber", service.random());
        return "numbers";
    }
}
