package ru.itis.di.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.itis.di.services.RandomNumbersService;
import ru.itis.di.services.RandomNumbersWithRangeServiceImpl;

import java.util.Random;

/**
 * 13.03.2021
 * 02. di-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Configuration
public class ApplicationConfig {

    @Bean
    public RandomNumbersService randomNumbersWithRangeService() {
        return new RandomNumbersWithRangeServiceImpl(100);
    }

    @Bean
    public Random random() {
        return new Random();
    }
}
