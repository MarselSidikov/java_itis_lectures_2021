package ru.itis.di.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * 13.03.2021
 * 02. di-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component("randomNumbersService")
public class RandomNumbersServiceImpl implements RandomNumbersService {

    @Autowired
    private Random random;

    @Override
    public int random() {
        return random.nextInt();
    }
}
