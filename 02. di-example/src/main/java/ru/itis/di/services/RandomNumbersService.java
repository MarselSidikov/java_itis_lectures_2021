package ru.itis.di.services;

/**
 * 13.03.2021
 * 02. di-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface RandomNumbersService {
    int random();
}
