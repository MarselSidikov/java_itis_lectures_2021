# REST API

* Нет понятия "сессия" и "состояние"

* Ориентация на ресурсные виды URL

* Используются HTTP-методы: GET, POST, PUT, DELETE

* JSON

```
GET /teachers/4/courses

GET /courses/5/lessons
```