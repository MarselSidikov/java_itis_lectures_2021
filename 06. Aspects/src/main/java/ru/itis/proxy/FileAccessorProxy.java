package ru.itis.proxy;

import java.io.InputStream;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileAccessorProxy extends FileAccessor {
    private FileAccessor target;

    private After after;
    private Before before;

    public FileAccessorProxy(FileAccessor target) {
        this.target = target;
    }

    @Override
    public InputStream openFile(String fileName) {
        if (before != null) {
            before.before();
        }
        InputStream result = target.openFile(fileName);
        if (after != null) {
            after.after();
        }
        return result;
    }

    public void setAfter(After after) {
        this.after = after;
    }

    public void setBefore(Before before) {
        this.before = before;
    }
}
