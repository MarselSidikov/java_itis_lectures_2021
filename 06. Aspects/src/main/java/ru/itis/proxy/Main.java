package ru.itis.proxy;

import java.io.InputStream;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        FileAccessor accessor = new FileAccessor();
        FileAccessorProxy accessorProxy = new FileAccessorProxy(accessor);
        accessorProxy.setBefore(() -> System.out.println("Открытие файла"));
        accessorProxy.setAfter(() -> System.out.println("Файл был открыт"));

        FileAccessor proxied = accessorProxy;

        // ----

        proxied.openFile("input.txt");
    }
}
