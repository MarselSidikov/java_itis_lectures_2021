package ru.itis.aop;

import org.springframework.aop.ThrowsAdvice;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThrowsOpenFile implements ThrowsAdvice {
    public void afterThrowing(Exception ex) {
        System.out.println("Было исключение " + ex.getMessage());
    }
}
