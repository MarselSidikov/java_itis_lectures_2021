package ru.itis.aop;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileAccessor {

    private LocalDateTime lastFileOpenTime;

    public InputStream openFile(String fileName) {
        try {
            lastFileOpenTime = LocalDateTime.now();
            return new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public LocalDateTime getLastFileOpenTime() {
        return lastFileOpenTime;
    }
}
