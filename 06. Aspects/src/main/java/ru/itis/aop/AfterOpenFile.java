package ru.itis.aop;

import org.springframework.aop.AfterAdvice;
import org.springframework.aop.AfterReturningAdvice;

import java.io.InputStream;
import java.lang.reflect.Method;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AfterOpenFile implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Был вызван метод " + method.getName());
        System.out.println("Открыт inputStream, сколько байт доступно " + ((InputStream)returnValue).available());
        System.out.println(((FileAccessor)(target)).getLastFileOpenTime());
    }
}
