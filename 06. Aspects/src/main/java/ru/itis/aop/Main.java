package ru.itis.aop;

import org.springframework.aop.framework.ProxyFactory;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        FileAccessor target = new FileAccessor();
        ProxyFactory proxyFactory = new ProxyFactory(target);
        proxyFactory.addAdvice(new BeforeOpenFile());
        proxyFactory.addAdvice(new AfterOpenFile());
        proxyFactory.addAdvice(new ThrowsOpenFile());

        FileAccessor proxied = (FileAccessor) proxyFactory.getProxy();
        proxied.openFile("input.txt");
    }
}
