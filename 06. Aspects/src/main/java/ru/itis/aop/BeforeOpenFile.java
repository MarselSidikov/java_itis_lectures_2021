package ru.itis.aop;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * 12.05.2021
 * 06. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BeforeOpenFile implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Целевой объект - " + target);
        System.out.println("Открытие файла - " + args[0]);
    }
}
