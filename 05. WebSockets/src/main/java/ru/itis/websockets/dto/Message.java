package ru.itis.websockets.dto;

import lombok.Data;

/**
 * 07.04.2021
 * 05. WebSockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class Message {
    private String text;
    private String from;
}
