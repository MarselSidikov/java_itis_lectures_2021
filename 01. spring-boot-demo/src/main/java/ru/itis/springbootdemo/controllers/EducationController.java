package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.springbootdemo.dto.CourseDto;
import ru.itis.springbootdemo.models.Lesson;
import ru.itis.springbootdemo.repositories.CoursesRepository;
import ru.itis.springbootdemo.repositories.LessonsRepository;
import ru.itis.springbootdemo.repositories.UsersRepository;

import java.util.List;

import static ru.itis.springbootdemo.dto.CourseDto.from;

/**
 * 24.02.2021
 * 01. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class EducationController {
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private LessonsRepository lessonsRepository;

    @Autowired
    private CoursesRepository coursesRepository;

    @GetMapping("/courses/byLessonId")
    public List<CourseDto> getCoursesByLessonId(@RequestParam("lesson_id") Long lessonId) {
//        Lesson lesson = lessonsRepository.getOne(lessonId);
//        return from(coursesRepository.findAllByLessonsContains(lesson));
        return from(coursesRepository.findByLessonId(lessonId));
    }

    @GetMapping("/courses/byTeacherId")
    public List<CourseDto> getCoursesByTeacherId(@RequestParam("teacherId") Long teacherId) {
        return from(coursesRepository.findAllByTeacher_Id(teacherId));
    }
}
